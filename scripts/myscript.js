


var add_weather = function(){

    // new element
    var new_card = document.getElementById('card_template').content.cloneNode(true);

    // modify
    new_card.querySelector(".location").innerHTML = getDialogOption("dialog_location_menu");


    // add to html
    var main = document.getElementsByTagName('main')[0];
    main.appendChild(new_card);
};

function getDialogOption(dialog_id){
    return document.getElementById(dialog_id).value;
}

var toggleDialog = function (dialog_id){
    return function(){
        var dialog = document.getElementById(dialog_id);
        if(dialog.style.display == "block")
           dialog.style.display = "none";
        else{
            dialog.style.display = "block";
            dialog.querySelector(".dialog_content").focus();
        }
    };
};

var displayRequestReport = (function(){
  var dialog = $('#dialog_request_report');
  dialog.on('animationend', function(e){
    if(e.animationName === 'fadeOut'){
      dialog.css({'display': 'none'});
      dialog.removeClass('fade_out');
      console.log("HI");
    }
  });
  var timeout;

  return function(text, color){
    if(timeout){
      clearTimeout(timeout);
      dialog.css({'display': 'none'});
      //dialog.removeClass('fade_out');
    }

    dialog.html(text);
    dialog.css({'display': 'inline', 'color':color});
    //dialog.addClass('fade_in');

    timeout = setTimeout(function () {
      //dialog.addClass('fade_out');
      dialog.css({'display':'none'});
    }, 2000);
  }
})();

var stopPropagation = function(event){
    event.stopPropagation();
};

var requestWeather = function(location, success, fail, invalid){
  // get location

  // make request + async loading screen
  var req = new XMLHttpRequest();
  req.onreadystatechange = function(){
    // finish
    if (req.readyState == 4){
      switch(req.status){
        case 200:
          success(JSON.parse(req.response));
          break;
        case 404:   // city not found
          invalid();
          break;
        default:
          fail();
      }
    }
  };

  req.open('GET', '/weathers?location='+location, true);
  req.send();
};

var addWeatherToPage = function(info){
  var name = info.name;
  var weather = info.weather[0].main;
  var iconID = info.weather[0].icon.substring(0,2);
  var humilidity = info.main.humilidity;
  var wind = info.wind.deg;
  var sunrise = new Date(info.sys.sunrise * 1000).toLocaleTimeString();
  var sunset = new Date(info.sys.sunset * 1000).toLocaleTimeString();
  var temp_min = info.main.temp_min;
  var temp_max = info.main.temp_max;
  var avgTemp = Math.round((temp_min+temp_max)/2);

  var newCard = $(document.getElementById('card_template').content.cloneNode(true));
  newCard.find('.location').eq(0).html(name);
  newCard.find('.conclusion').eq(0).html(weather);
  newCard.find('.time').eq(0).html(new Date().toLocaleTimeString());

  var main = newCard.find('.main_report').eq(0);
  main.find('.temperature_img').eq(0).html(`<img src="images/weather_icons/${iconID}.png">`);
  main.find('.humilidity').eq(0).html(humilidity);
  main.find('.wind .value').eq(0).html(wind);
  main.find('.sunrise .value').eq(0).html(sunrise);
  main.find('.sunset .value').eq(0).html(sunset);
  main.find('.temperature .value').eq(0).html(`${avgTemp}`);

  $('main').append(newCard);

  displayRequestReport("Location added", 'green');
};

var failFetch = function(){
  console.log("FAILED FETCH");
};
var invalidFetch = function(){
  displayRequestReport("Invalid Location", 'red');
};



var populateCityOptions = function(){

  var req = new XMLHttpRequest();
  req.open('GET', '/json/locations.json', true);
  req.onreadystatechange = function(){
    if(req.readyState == 4 && req.status == 200){
      var res = JSON.parse(req.response);
      var locations = res.locations;

      for(let i=0; i<locations.length; i++){
        var newOption = document.createElement('option');
        newOption.value = locations[i];
        newOption.innerHTML = locations[i];

        $('#dialog_location_menu').append($(newOption));
      }
    }
  };
  req.send();
};

function init(){
    populateCityOptions();

    // location button
    $('#dialog_location_add').on("click", function(){
      requestWeather(getDialogOption('dialog_location_menu'), addWeatherToPage, failFetch, invalidFetch);
    });

    // plus btn
    document.getElementById("addBtn").onclick = toggleDialog("location_select");

    // dialogs
    var dialogs = document.getElementsByClassName("dialog");
    for(var i = 0; i < dialogs.length; i++){
        var id = dialogs[i].id;
        var dialog_content = dialogs[i].querySelector(".dialog_content");
        var close = dialogs[i].querySelector(".dialog_close");

        close.onclick = toggleDialog(id);
        dialogs[i].onclick = toggleDialog(id);
        dialog_content.addEventListener("click", stopPropagation);
    }

}
