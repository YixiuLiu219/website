const http = require('http');
const fs = require('fs');
const path = require('path');
const request = require('request');

const ip = '127.0.0.1';
const port = 3000;

const httpOK = 200;
const httpNotFound = 404;
const httpBadRequest = 400

const extension = '.';

const doctypes = {
    '.js' : 'text/javascript',
    '.css' : 'text/css',
    '.html' : 'text/html',
    '.ico' : 'image/x-con',
    '.svg' : 'image/svg+xml',
    '.json' : 'application/json',
    _plain : 'text/plain'
}

const doctype = function(extension){
    return doctypes[extension] || doctypes._plain;
}

const relative = function(url){
  return path.join(__dirname, '../', url);
}


const querySymbol = '?';
const paramValueSymbol = '=';
const paramSeparationSymbol = '&';

const appID  = "978b0adcad61e40e6f13ad361c28578c";



var parseUrl = function(url){
  var url = decodeURI(url);
  var paramSplit = url.split(querySymbol);
  var params = {};
  var paramString = paramSplit[1].split(paramSeparationSymbol);
  for(let i=0; i<paramString.length; i++){
    var keyVal = paramString[i].split(paramValueSymbol);
    params[keyVal[0]] = keyVal[1];
  }

  return {
    route : paramSplit[0],
    parameters : params
  };
}

var weatherAPIRequest = function(location, callback){
  var url = `http://api.openweathermap.org/data/2.5/weather?q=${location}&APPID=${appID}`;
  request(encodeURI(url), callback);
};

var handleQuery = function(req, res, invalidPage){
  //parse
  var parsedUrl = parseUrl(req.url);
  var params = parsedUrl.parameters;
  var route = parsedUrl.route;
  console.log(route);
  console.log(JSON.stringify(params));

  //request weather api
  weatherAPIRequest(params.location, function(err, weatherResponse, body){
    if(err){
      console.log(err);
      res.statusCode = httpBadRequest;
      res.setHeader('Content-type', doctype('plain'));
      res.end();
    }
    else{
      res.statusCode = weatherResponse.statusCode;
      res.setHeader('Content-type', doctype('.json'));
      res.write(body);
      res.end();
    }
  });
};

var handleRouting = function(req, res, invalidPage){

  var index = req.url.lastIndexOf(extension);
  var type = doctype(req.url.substring(index));
  var url = relative(req.url);

  console.log("Request: " + req.method + " "+req.url);

  fs.readFile(url, (err, data)=>{
      if(err){
          res.statusCode = httpNotFound;
          res.setHeader('Content-type', 'text/html');
          res.write(invalidPage)

          console.log("Error in request: "+url);
      }
      else{
          res.setHeader('Content-type', type);
          res.statuCode = httpOK;
          res.write(data);
      }
      res.end();
  });
};

var testHandler = function(req, res, invalid){

      fs.readFile(relative('/json/test.json'), function(err, content){
          res.setHeader('Content-type', doctype('.json'));
          res.statuCode = httpOK;
          res.write(content);
          res.end()

      });

}

var createRequestHandler = function(invalidPage){
  var requestHandler = function(req, res){

      var urlSplit = req.url.split(querySymbol);
      if(urlSplit.length>=2){ //handleQuery
        handleQuery(req, res, invalidPage);
        //testHandler(req, res, invalidPage);
      }
      else{ //route
        handleRouting(req, res, invalidPage);
      }
  };
  return requestHandler;
};

const exit = function(message){
  console.log(message);
  console.log("Exiting...")
  process.exit();
};
// PROBLEM
  // data is in above function, the data's value is determined in callback function
    // how to make the data in above function refer to the value in the callback?
const serverStart = function(invalidPage){
  const server = http.createServer(createRequestHandler(invalidPage));
  server.listen(port, ip, ()=>{
      console.log("Server started at port "+port);
  });
};

var load404HTML = function(callback){
  fs.readFile(relative('404.html'), (err, data)=>{
    if(err){
      exit("Fail to load 404 html");
    }
    callback(data);
  });
};


load404HTML(serverStart);
